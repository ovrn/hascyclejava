/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ovrn.linkedlist;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Oleg
 */
public class ListFactoryTest {

    private int countResultLength(List listHead, int expectedLength){
        int resultLenght = 1;
        while(listHead.hasNext()){
            listHead = listHead.next();
            resultLenght++;
            assertFalse(resultLenght > expectedLength);
        }
        
        return resultLenght;
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testGenerateLinkedList_int_WrongArguments(){
        System.out.println("generateLinkedList without loop, wrong argument");
        ListFactory instance = new ListFactory();
        
        instance.generateLinkedList(0);
    }
    
    @Test
    public void testGenerateLinkedList_int() {
        System.out.println("generateLinkedList without loop");
        ListFactory instance = new ListFactory();
        List list;
        int length;
        int resultLenght;
        
        length = 1;
        list = instance.generateLinkedList(length);
        resultLenght = countResultLength(list, length);        
        assertEquals(resultLenght, length);
        
        length = 20;
        list = instance.generateLinkedList(length);
        resultLenght = countResultLength(list, length);        
        assertEquals(resultLenght, length);
        
        length = 2000000;
        list = instance.generateLinkedList(length);
        resultLenght = countResultLength(list, length);        
        assertEquals(resultLenght, length);
    }

    private List getNode(List listHead, int index){
        if(index == 0)
            return listHead;
        
        List list = listHead;
        for(int i=1; i <= index; i++){
            if(!list.hasNext())
                throw new IndexOutOfBoundsException();
            list = list.next();
        }
        
        return list;
    }
    
    @Test
    public void testGenerateLinkedList_int_int() {
        System.out.println("generateLinkedList with loop");
        ListFactory instance = new ListFactory();
        List list;
        int length;
        int loopIndex;
        List lastNode;
        List loopNode;
        
        
        length = 0;
        loopIndex = 0;
        try{
            list = instance.generateLinkedList(length, loopIndex);
            fail("No exception rised");
        } catch(IllegalArgumentException ex){
        }
        
        length = 1;
        loopIndex = 5;
        try{
            list = instance.generateLinkedList(length, loopIndex);
            fail("No exception rised");
        } catch(IndexOutOfBoundsException ex){
        }
        
        length = 10;
        loopIndex = 5;
        list = instance.generateLinkedList(length, loopIndex);
        lastNode = getNode(list, length-1);
        loopNode = getNode(list, loopIndex);
        assertEquals(loopNode, lastNode.next());
        
        length = 200;
        loopIndex = length-1;
        list = instance.generateLinkedList(length, loopIndex);
        lastNode = getNode(list, length-1);
        assertEquals(lastNode, lastNode.next());
        
        length = 2000000;
        loopIndex = 0;
        list = instance.generateLinkedList(length, loopIndex);
        lastNode = getNode(list, length-1);
        assertEquals(list, lastNode.next());
    }
    
}
