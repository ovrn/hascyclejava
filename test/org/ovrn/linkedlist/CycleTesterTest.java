/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ovrn.linkedlist;

import java.util.function.Function;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Oleg
 */
public class CycleTesterTest {
    private final ListFactory<Integer> listFactory = new ListFactory<>();
    private final CycleTester cycleTester = new CycleTester();

    private void testAlgorithm(Function<List, Boolean> function){
        List<Integer> list;
        boolean result;
        
        list = listFactory.generateLinkedList(1);
        result = function.apply(list);
        assertEquals(false, result);
        
        list = listFactory.generateLinkedList(2000);
        result = function.apply(list);
        assertEquals(false, result);

        list = listFactory.generateLinkedList(1, 0);
        result = function.apply(list);
        assertEquals(true, result);        

        list = listFactory.generateLinkedList(2000, 0);
        result = function.apply(list);
        assertEquals(true, result);
        
        list = listFactory.generateLinkedList(251, 250);
        result = function.apply(list);
        assertEquals(true, result);
        
        list = listFactory.generateLinkedList(200000, 10999);
        result = function.apply(list);
        assertEquals(true, result);
    }
    
    @Test
    public void testHasCycle() {
        System.out.println("hasCycle");

        testAlgorithm((List t) -> {
            return cycleTester.hasCycle(t);
        });
    }

    @Test
    public void testhasLoopFloyd() {
        System.out.println("hasLoopFloyd");
        
        testAlgorithm((List t) -> {
            return cycleTester.hasLoopFloyd(t);
        });
    }

    @Test
    public void testHasLoopBrent() {
        System.out.println("hasLoopBrent");
        
        testAlgorithm((List t) -> {
            return cycleTester.hasLoopBrent(t);
        });
    }
    
}
