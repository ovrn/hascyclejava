/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ovrn.linkedlist;

/**
 *
 * @author Oleg
 */
public class CycleTester {
    
    // My version based on Floyd’s Cycle-Finding Algorithm
    public boolean hasCycle(List list){
        List loop1 = list;
        List loop2 = list;
        
        while(loop2.hasNext()){
            loop2 = loop2.next();
            
            if(!loop2.hasNext())
                return false;
        
            loop2 = loop2.next();
            if(loop1 == loop2)
                return true;
            loop1 = loop1.next();
        }
        
        return false;
    }
    
    // Floyd’s Cycle-Finding Algorithm better implementation 
    public boolean hasLoopFloyd(List startNode){
        List slowNode = startNode;
        List fastNode1 = startNode;
        List fastNode2 = startNode;
        
        if(!slowNode.hasNext())
            return false;
        
        while (fastNode2.hasNext()){
            fastNode1 = fastNode2.next();
            if(!fastNode1.hasNext())
                return false;
            fastNode2 = fastNode1.next();
            
            if(slowNode == fastNode1 || slowNode == fastNode2) 
                return true;
            
            slowNode = slowNode.next();
        }
        return false;
    }
    
    // Brent's Cycle-Finding Algorithm
    public boolean hasLoopBrent(List startNode){
        List currentNode = startNode;
        List checkNode = null;
        int since = 0;
        int sinceScale = 2;
        
        if(!currentNode.hasNext())
            return false;        
        do {
          if (checkNode == currentNode) 
              return true;
          if (since >= sinceScale){
              checkNode = currentNode;
              since = 0;
              sinceScale = 2*sinceScale;
          }
          since++;
          currentNode = currentNode.next();
        } while (currentNode.hasNext());
        
        return false;
    }
    
}
