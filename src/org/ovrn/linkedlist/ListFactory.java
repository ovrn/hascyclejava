/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ovrn.linkedlist;

/**
 *
 * @author Oleg
 * @param <T>
 */
public class ListFactory<T> {
    
    public List<T> generateLinkedList(int length) {
        if(length <= 0)
            throw new IllegalArgumentException("Length must be greater then 0");
        
        LinkedList<T> listHead = new LinkedList<>();
        LinkedList<T> list = listHead;
        for(int i=1; i < length; i++){
            list = list.addNext(new LinkedList<>());
        }
        
        return listHead;
    }
    
    public List<T> generateLinkedList(int length, int loopIndex){
        if(length <= 0)
            throw new IllegalArgumentException("length must be more then 0");
        
        if(loopIndex < 0 || loopIndex >= length)
            throw new IndexOutOfBoundsException("loopIndex can't be more then lenght and less then 0");
        
        LinkedList<T> listHead = new LinkedList<>();
        LinkedList<T> list = listHead;
        LinkedList<T> loopNode = listHead;
        for(int i=1; i < length; i++){
            list = list.addNext(new LinkedList<>());
            if(i == loopIndex)
                loopNode = list;
        }
        list.addNext(loopNode);
        
        return listHead;
    }
    
}
