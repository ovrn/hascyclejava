/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ovrn.linkedlist;

/**
 *
 * @author Oleg
 * @param <T>
 */
public class LinkedList<T> implements List<T>{
    private LinkedList<T> next;
    private T value;
    
    public LinkedList(){
        
    }
    
    public LinkedList(T value){
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public LinkedList<T> addNext(LinkedList<T> next) {
        this.next = next;
        return next;
    }
    
    @Override
    public List<T> next() {
        return next;
    }

    @Override
    public boolean hasNext() {
        return next != null;
    }
    
}
