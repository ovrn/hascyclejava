/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ovrn.linkedlist;

/**
 *
 * @author Oleg
 * @param <T>
 */
public interface List<T> {
    
    public List<T> next();
    public boolean hasNext();
    
}
